using UnityEngine;

namespace Wander
{
    public class DirectionIndicator : MonoBehaviour
    {
        [SerializeField] private Camera mainCamera = null;

        [SerializeField] private ArrowBehaviour arrowBehaviour = ArrowBehaviour.Circular;
        [SerializeField] private Transform arrowTransform = null;
        [SerializeField] private ArrowVisual arrowGroup = null;

        private Vector3 targetPosition = Vector3.zero;
        private bool guidanceEnabled = false;

        /// <summary>
        /// Start the guidance until EndGuidance is called
        /// </summary>
        /// <param name="pTargetPosition">The target position</param>
        public void StartGuidance(Vector3 pTargetPosition, Quaternion pTargetRotation = default)
        {
            targetPosition = pTargetPosition;

            EndGuidance();
            guidanceEnabled = true;
        }

        /// <summary>
        /// Shart the guidance until EndGuidance is called
        /// </summary>
        /// <param name="pTargetTransform">The target Transform</param>
        public void StartGuidance(Transform pTargetTransform)
        {
            StartGuidance(pTargetTransform.position, pTargetTransform.rotation);
        }

        /// <summary>
        /// Ends the guidance
        /// </summary>
        public void EndGuidance()
        {
            guidanceEnabled = false;
            arrowGroup.SetVisualsActive(0);
        }

        private void Update()
        {
            if (guidanceEnabled)
            {
                //Get the direction of the target
                Vector3 _targetDirection = targetPosition - mainCamera.transform.position;

                //Gets the angle between the camera forward and the target position
                float _distanceToTarget = Vector3.SignedAngle(_targetDirection, mainCamera.transform.forward, Vector3.up);

                //arrows turn on by distance
                arrowGroup.SetVisualsActive(_distanceToTarget);

                switch (arrowBehaviour)
                {
                    case ArrowBehaviour.Circular:
                        RotateArrowsTowardsTarget();
                        break;
                    case ArrowBehaviour.Horizontal:
                        RotateArrowsHorizontal(_distanceToTarget >= 0);
                        break;
                }
            }
        }

        /// <summary>
        /// Rotates the arrow in the direction of the target
        /// </summary>
        private void RotateArrowsTowardsTarget()
        {
            //get targetpos relative to player
            Vector3 _targetPosLocal = mainCamera.transform.InverseTransformPoint(targetPosition);

            //calculate the angle
            float _targetAngle = (-Mathf.Atan2(_targetPosLocal.x, _targetPosLocal.y) * Mathf.Rad2Deg) - 90;

            //set the rotation to the angle
            arrowTransform.localEulerAngles = new Vector3(0, 0, _targetAngle);
        }

        /// <summary>
        /// Flips the arrow directions on the horizontal axis
        /// </summary>
        /// <param name="pDistanceToTarget">Target is to the left of the user</param>
        private void RotateArrowsHorizontal(bool pTargetIsLeft)
        {
            arrowTransform.localEulerAngles = new Vector3(0, 0, pTargetIsLeft ? 0 : 180);

        }

        private enum ArrowBehaviour
        {
            Circular,
            Horizontal
        }
    }
}