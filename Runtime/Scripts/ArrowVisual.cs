using System;
using UnityEngine;

namespace Wander
{

    /// <summary>
    /// Object to more easily manage the arrow groups
    /// </summary>
    [Serializable]
    public class ArrowVisual
    {
        [SerializeField] private ArrowBoundarySet smallArrowVisual = new ArrowBoundarySet(null, 15f);
        [SerializeField] private ArrowBoundarySet mediumArrowVisual = new ArrowBoundarySet(null, 60f);
        [SerializeField] private ArrowBoundarySet largeArrowVisual = new ArrowBoundarySet(null, 120f);

        public void SetVisualsActive(float pDistance)
        {
            if (pDistance >= 0)
            {
                SetVisualsActive(
                    pDistance > smallArrowVisual.ArrowBoundary,
                    pDistance > mediumArrowVisual.ArrowBoundary,
                    pDistance > largeArrowVisual.ArrowBoundary);
            }
            else
            {
                SetVisualsActive(
                    pDistance < -smallArrowVisual.ArrowBoundary,
                    pDistance < -mediumArrowVisual.ArrowBoundary,
                    pDistance < -largeArrowVisual.ArrowBoundary);
            }
        }

        private void SetVisualsActive(bool pSmallActive, bool pMediumActive, bool pLargeActive)
        {
            smallArrowVisual.ArrowVisual.SetActive(pSmallActive);
            mediumArrowVisual.ArrowVisual.SetActive(pMediumActive);
            largeArrowVisual.ArrowVisual.SetActive(pLargeActive);
        }

        [Serializable]
        public class ArrowBoundarySet
        {
            public GameObject ArrowVisual;
            public float ArrowBoundary;

            public ArrowBoundarySet(GameObject arrowVisual, float arrowBoundary)
            {
                ArrowVisual = arrowVisual;
                ArrowBoundary = arrowBoundary;
            }
        }
    }
}