# Direction Indicator

This package holds the direction indicator as seen in the CoHeSIVE & Castor projects

## Usage:
- Add the DirectionIndicatorCanvas to the scene
- Assign the Main Camera in the inspector
- Select a behaviour (Circular/Horizontal)
- Call StartGuidance(Vector3 position, Quaternion rotation) or StartGuidance(Tranform transform) in DirectionIndicator.cs
- Call EndGuidance() to end the guidance